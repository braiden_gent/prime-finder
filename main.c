//
// Created by YayIguess on 09/12/15.
// Originally made for https://projecteuler.net/problem=7
//

#include <stdio.h>

#define PRIME_WE_WANT 10000

long prime_finder(void);

int main(void)
{
	char printf_val[10] = {0};
	sprintf(printf_val,"%d", PRIME_WE_WANT);

	printf("%s: %ld", printf_val, prime_finder());

	return 0;
}

long prime_finder(void)
{
	int total_primes_found = 0;

	long primes[PRIME_WE_WANT] = {0};

	for(int i = 0;i < PRIME_WE_WANT; i++) //set everything to 0
	{
		primes[i] = 0;
	}

	for(long i = 1; total_primes_found < PRIME_WE_WANT;i++) //find the primes
	{
		if(i<14) //cheat a little for the first primes
		{
			switch (i)
			{
				case 1:
					break;

				case 2:
					primes[total_primes_found++] = 2;
					break;

				case 3:
					primes[total_primes_found++] = 3;
					break;

				case 5:
					primes[total_primes_found++] = 5;
					break;

				case 7:
					primes[total_primes_found++] = 7;
					break;

				case 13:
					primes[total_primes_found++] = 13;
					break;

				default:
					break;
			}
		}

		else //if it is a larger number
		{
			if (i % 2 == 0 || i % 3 == 0 || i % 5 == 0 || i % 7 == 0 || i % 13 == 0) /*makes the program a little quicker if the
                                                                                        *current number divides by a
                                                                                        *low number like 2 or 3*/
			{
				goto End;
			}

			else
			{
				for(int j = 0; j < total_primes_found; j++) //brute force part
				{
					if(i % primes[j] == 0)
					{
						goto End;
					}
				}

				primes[total_primes_found++] = i;
			}

			End:;

		}
	}

	for(int i = 0 ; i < PRIME_WE_WANT - 1 ; i++)
		printf("%d: %ld\n", i+1, primes[i]);

	return primes[PRIME_WE_WANT - 1];
}

